from aiohttp import web
from pymongo import MongoClient
from utils import log


class Server:

    def __init__(self, configuration, estimator):
        # estimator
        self._estimator = estimator
        # app
        self._app = web.Application()
        self._app.router.add_post(configuration['service'], self.handler)
        self._host = configuration['host']
        self._port = configuration['port']
        # mongo
        self._client = MongoClient(configuration['mongo_host'], configuration['mongo_port'])
        self._db = self._client[configuration['mongo_db']]
        self._collection = self._db[configuration['mongo_collection']]

    def run(self):
        """
        Run the server
        """
        web.run_app(self._app, host=self._host, port=self._port)

    async def handler(self, request):
        """
        Handler
        """
        req_data = await request.json()
        log('request')

        err_msg = ''
        if 'mention_text' not in req_data:
            err_msg += 'No parameter "mention_text"! '
        if 'exp_mprofile' not in req_data:
            err_msg += 'No parameter "exp_mprofile"! '
        if 'mprofile_id' not in req_data:
            err_msg += 'No parameter "mprofile_id"! '
        if 'post_type' not in req_data:
            err_msg += 'No parameter "post_type"! '
        if 'post_id' not in req_data:
            err_msg += 'No parameter "post_id"! '
        if err_msg != '':
            log('response', 400, err_msg)
            return web.json_response(err_msg, status=400)

        estimation = self._estimator.estimate(req_data)
        resp = {
            "uid": req_data['mprofile_id'],
            "message": req_data['mention_text'],
            "importance": estimation,
            "post_type": req_data['post_type'],
            "post_id": req_data['post_id']
        }
        log('mongo add')
        self._collection.insert_one(resp)

        return web.json_response(status=200)
