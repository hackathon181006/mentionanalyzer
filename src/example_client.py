import json
import requests
from pymongo import MongoClient

resp = requests.post('http://localhost:8088/analyze_mention/', json.dumps({
    'mention_text': 'текст',
    'exp_mprofile': 0,
    'mprofile_id': 'uid'
}))
print(resp.status_code)
print(resp.text)

client = MongoClient()
db = client.MegafonHackathon
collection = db.Messages

for obj in collection.find():
    print(obj)
