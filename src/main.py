from estimator import Estimator
from server import Server

server_configuration = {
    'host': '0.0.0.0',
    'port': 8088,
    'service': '/analyze_mention/',
    'mongo_host': 'localhost',
    'mongo_port': 27017,
    'mongo_db': 'MegafonHackathon',
    'mongo_collection': 'Messages'
}


def run_app():
    estimator = Estimator()
    server = Server(server_configuration, estimator)
    server.run()


if __name__ == "__main__":
    run_app()
