import datetime


def log(*args):
    date_time = f"{datetime.datetime.now():%Y-%m-%d %H:%M:%S.%f}"[:-3]
    print(date_time, *args, sep=' - ')
