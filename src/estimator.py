import pickle
import pymorphy2
import os


class Estimator:

    def __init__(self):
        self.morph = pymorphy2.MorphAnalyzer()
        print(os.listdir('.'))
        print(os.listdir('..'))
        self.selector = pickle.load(open('../model/selector', 'rb'))
        self.vectorizer = pickle.load(open('../model/vectorizer', 'rb'))
        self.clf = pickle.load(open('../model/model_nb', 'rb'))

    def estimate(self, data):
        """
        The method estimetes the mention of the post
        """
        return self.handle_one(data['mention_text'])

    def handle_one(self, text):
        processed_text = [self.text_cleaner(text)]
        X = self.vectorizer.transform(processed_text)
        X = self.selector.transform(X)
        y = self.clf.predict_proba(X)
        return y[0][0]

    def text_cleaner(self, text):
        # к нижнему регистру
        text = text.lower()

        # оставляем в предложении только русские буквы (таким образом
        # удалим и ссылки, и имена пользователей, и пунктуацию и т.д.)
        alph = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя'

        cleaned_text = ''
        for char in text:
            if (char.isalpha() and char[0] in alph) or (char == ' '):
                cleaned_text += char

        result = []
        for word in cleaned_text.split():
            # лемматизируем
            result.append(self.morph.parse(word)[0].normal_form)

        return ' '.join(result)